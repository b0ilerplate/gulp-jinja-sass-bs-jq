/* 
 * Script Author: Debashis Roy Bhowmik
 * Author email: debashis.buet08@gmail.com
 * Website: https://debashis.me/
 */

const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const sass = require('gulp-sass');
const jinja = require('gulp-nunjucks');
const rename = require('gulp-rename');
const auto_prefixer = require('gulp-autoprefixer');
const browser_sync = require('browser-sync').create();


const copyOtherAssetsToBuild = g => g.src(['src/other-assets/**/*']).pipe(gulp.dest('./build'));

const templateRenderer = g => g
  .src(['src/jinja/*.html', '!src/jinja/_*.html'])
  .pipe(jinja.compile())
  .pipe(plumber({
    errorHandler: notify.onError({
      title: 'Jinja',
      message: 'Error: <%= error.message %>'
    })
  }))
  .pipe(plumber.stop())
  .pipe(gulp.dest('./build'))
  .pipe(browser_sync.stream());

const styleRenderer = g => g
  .src('./src/scss/*.scss')

  // error message handler
  .pipe(plumber({
    // show notification
    errorHandler: notify.onError({
      title: 'SASS',
      message: 'Error: <%= error.message %>'
    })
  }))

  // pass scss files to sass compiler
  .pipe(sass({
    // outputStyle:'expanded'
    outputStyle: 'compressed'
  }))

  // css prefix for browsers compatibility (browsers listed on package.json)
  .pipe(auto_prefixer())

  // rename output file
  .pipe(rename({suffix: '.min'}))

  // default behaviour for pipeline after it was piped
  .pipe(plumber.stop())

  // set compiled css file location
  .pipe(gulp.dest('./build/css'))

  // stream changes to browser
  .pipe(browser_sync.stream());


const template = () => templateRenderer(gulp);
const style = () => styleRenderer(gulp);
const copyAssets = () => copyOtherAssetsToBuild(gulp);

const watch = () => {
  // initial process
  template();
  style();
  copyAssets();

  browser_sync.init({
    server: {
      baseDir: './build'
    }
  });

  // watch for these file changes
  gulp.watch('./src/scss/**/*.scss', style);
  gulp.watch('./src/jinja/**/*.html', template);
  gulp.watch('./src/other-assets/**/*', copyAssets);
  gulp.watch('./build/*.js').on('change', browser_sync.reload);
};

const build = () => templateRenderer(gulp).on('end', () => styleRenderer(gulp)).on('end', () => copyOtherAssetsToBuild(gulp));

exports.style = style;
exports.template = template;
exports.watch = watch;
exports.build = build;
