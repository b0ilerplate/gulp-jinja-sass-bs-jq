# Gulp setup for jinja and sass/scss

## Framework/library used ##
* `node`: `> 10`
* `npm`: `> 7`
* `bootstrap`: `4.4`
* `gulp`: 
  * cli: `> 2`
  * local: `> 4`


## Development ##
* `sass` compile: `npm run css`
* `jinja` compile: `npm run html`
* live server with browser reload: `npm run start` or `npm start`
* build files: `npm run build`
